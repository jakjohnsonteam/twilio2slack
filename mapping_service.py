from database import db_session

from models import NumberToChannelMapping


class MappingService:
    def __init__(self, logger):
        self.logger = logger

    def get_mapping_from_slack_channel_id(self, slack_channel_id):
        number_to_channel_mapping = NumberToChannelMapping.query.filter_by(channel_id=slack_channel_id).first()

        if number_to_channel_mapping:
            self.logger.debug("Retrieved mapping %s with Slack channel_id %s",
                              number_to_channel_mapping,
                              slack_channel_id)
        else:
            self.logger.debug("No mapping exists for Slack channel_id %s. Could be general or random channel",
                              slack_channel_id)

        return number_to_channel_mapping

    def get_mapping_from_number(self, number):
        number_to_channel_mapping = NumberToChannelMapping.query.filter_by(number=number).first()

        if number_to_channel_mapping:
            self.logger.debug("Retrieved mapping %s with number %s",
                              number_to_channel_mapping,
                              number)
        else:
            self.logger.debug("No mapping exists for number %s",
                              number)

        return number_to_channel_mapping

    def add_mapping(self, number, channel_id):
        number_to_channel_mapping = NumberToChannelMapping(number, channel_id)
        db_session.add(number_to_channel_mapping)
        db_session.commit()
        self.logger.debug("Created channel mapping %s", number_to_channel_mapping)
        return number_to_channel_mapping

    def update_mapping_channel_id(self, number, new_channel_id):
        number_to_channel_mapping = self.get_mapping_from_number(number)
        old_channel_id = number_to_channel_mapping.channel_id
        number_to_channel_mapping.channel_id = new_channel_id
        db_session.commit()
        self.logger.debug("Updated channel mapping for %s, old channel %s -> new channel %s",
                          number, old_channel_id, new_channel_id)
        return number_to_channel_mapping

    def delete_mapping_by_channel_id(self, channel_id):
        number_to_channel_mapping = self.get_mapping_from_slack_channel_id(channel_id)
        if number_to_channel_mapping:
            db_session.delete(number_to_channel_mapping)
            db_session.commit()
            self.logger.debug("Deleted mapping %s", number_to_channel_mapping)
        else:
            self.logger.debug("No channel found, no deletion")
