import json
from base64 import b64encode
from logging import Logger

import slackclient
from mock import mock, call
from twilio.base.exceptions import TwilioRestException

import twilio2slack
import unittest

from models import NumberToChannelMapping
from slack_service import SlackChannel, SlackChannelNameTakenException, SlackChannelNotFoundException, \
    SlackChannelArchivedException, SlackService, SlackException, SlackChannelCreationException, SlackUser
from twilio_service import TwilioService

headers = {
    'Authorization': 'Basic ' + b64encode("{0}:{1}".format('twilio2slack', 'updateme'))
}


# noinspection PyUnresolvedReferences
class AppTestCase(unittest.TestCase):
    def setUp(self):
        self.app = twilio2slack.app.test_client()

    """
        Web server tests
    """

    @mock.patch('twilio2slack.mapping_service')
    @mock.patch('twilio2slack.slack_service')
    def test_first_sms(self, mock_slack_service, mock_mapping_service):
        """ Test simulating first SMS from a number. New mapping should be created, Slack channel should be created
        with all team members and message should be posted to Slack"""
        mock_mapping_service.get_mapping_from_number.return_value = None
        mock_mapping_service.add_mapping.return_value = NumberToChannelMapping('+34111111111', 'ABCDEFG')
        mock_slack_service.create_slack_channel.return_value = SlackChannel('ABCDEFG', '34111111111')

        expected_users = [SlackUser('AAAAA', 'jajohnso'), SlackUser('BBBBB', 'test')]
        mock_slack_service.list_users.return_value = expected_users

        twilio_data = {
            'From': '+34111111111',
            'Body': 'Hello, World'

        }

        response = self.app.post('/sms', data=twilio_data, headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_number.assert_called_with('+34111111111')
        mock_slack_service.create_slack_channel.assert_called_with('34111111111')
        mock_slack_service.add_users_to_channel.assert_called_with(expected_users, 'ABCDEFG')
        mock_mapping_service.add_mapping.assert_called_with('+34111111111', 'ABCDEFG')
        mock_slack_service.post_slack_message.assert_called_with('ABCDEFG', 'Hello, World')

    @mock.patch('twilio2slack.mapping_service')
    @mock.patch('twilio2slack.slack_service')
    def test_first_sms_list_user_fails(self, mock_slack_service, mock_mapping_service):
        """ Test simulating first SMS from a number. New mapping should be created, list team members fails but
        Slack channel should be created and message should be posted"""
        mock_mapping_service.get_mapping_from_number.return_value = None
        mock_mapping_service.add_mapping.return_value = NumberToChannelMapping('+34111111111', 'ABCDEFG')
        mock_slack_service.create_slack_channel.return_value = SlackChannel('ABCDEFG', '34111111111')
        mock_slack_service.add_users_to_channel.side_effect = SlackException()

        expected_users = [SlackUser('AAAAA', 'jajohnso'), SlackUser('BBBBB', 'test')]
        mock_slack_service.list_users.return_value = expected_users

        twilio_data = {
            'From': '+34111111111',
            'Body': 'Hello, World'

        }

        response = self.app.post('/sms', data=twilio_data, headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_number.assert_called_with('+34111111111')
        mock_slack_service.create_slack_channel.assert_called_with('34111111111')
        mock_slack_service.add_users_to_channel.assert_called_with(expected_users, 'ABCDEFG')
        mock_mapping_service.add_mapping.assert_called_with('+34111111111', 'ABCDEFG')
        mock_slack_service.post_slack_message.assert_called_with('ABCDEFG', 'Hello, World')

    @mock.patch('twilio2slack.mapping_service')
    @mock.patch('twilio2slack.slack_service')
    def test_nth_sms(self, mock_slack_service, mock_mapping_service):
        """ Test simulating nth message from a number. Existing mapping should be retrieved from the DB and message
        should be posted to Slack using the ID mapped to the number"""
        mock_mapping_service.get_mapping_from_number.return_value = NumberToChannelMapping('+34111111111', 'HIJKL')

        twilio_data = {
            'From': '+34111111111',
            'Body': 'Hello, World'

        }

        response = self.app.post('/sms', data=twilio_data, headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_number.assert_called_with('+34111111111')
        mock_slack_service.post_slack_message.assert_called_with('HIJKL', 'Hello, World')

    @mock.patch('twilio2slack.mapping_service')
    @mock.patch('twilio2slack.slack_service')
    def test_mapping_out_of_sync_channel_already_exists(self, mock_slack_service, mock_mapping_service):
        """ Test simulating a de-synchronization between mapping database and Slack. No mapping exists in DB so we
        will try to create a new Slack channel with name=number. This channel name already exists in Slack. We should
        recover successfully by getting the ID of that channel from Slack and creating a new mapping in DB. Message
        should be posted to Slack using the pre-existing channel id"""
        mock_mapping_service.get_mapping_from_number.return_value = None
        mock_slack_service.create_slack_channel.side_effect = SlackChannelNameTakenException()
        mock_slack_service.list_slack_channels.return_value = [SlackChannel('ABCD', '34111111111'),
                                                               SlackChannel('EFGH', '34111111112')]
        mock_mapping_service.add_mapping.return_value = NumberToChannelMapping('+34111111111', 'ABCD')

        twilio_data = {
            'From': '+34111111111',
            'Body': 'Hello, World'

        }

        response = self.app.post('/sms', data=twilio_data, headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_number.assert_called_with('+34111111111')
        mock_slack_service.create_slack_channel.assert_called_with('34111111111')
        mock_slack_service.list_slack_channels.assert_called()
        mock_mapping_service.add_mapping.assert_called_with('+34111111111', 'ABCD')
        mock_slack_service.post_slack_message.assert_called_with('ABCD', 'Hello, World')

    @mock.patch('twilio2slack.mapping_service')
    @mock.patch('twilio2slack.slack_service')
    def test_mapping_out_of_sync_channel_not_found(self, mock_slack_service, mock_mapping_service):
        """ Test simulating a de-synchronization between mapping database and Slack. Mapping exists in DB so we
        will try to post to a Slack channel with that ID. This channel ID does not exists in Slack. We should
        recover successfully by creating a new Slack channel, adding all team members and updating the mapping.
        Message should be posted to Slack using the new channel id"""
        mock_mapping_service.get_mapping_from_number.return_value = NumberToChannelMapping('+34111111111', 'ABCD')
        mock_slack_service.post_slack_message.side_effect = [SlackChannelNotFoundException(), None]
        mock_slack_service.create_slack_channel.return_value = SlackChannel('EFGH', 34111111111)

        expected_users = [SlackUser('AAAAA', 'jajohnso'), SlackUser('BBBBB', 'test')]
        mock_slack_service.list_users.return_value = expected_users

        mock_mapping_service.update_mapping_channel_id.return_value = NumberToChannelMapping('+34111111111', 'EFGH')

        twilio_data = {
            'From': '+34111111111',
            'Body': 'Hello, World'

        }

        response = self.app.post('/sms', data=twilio_data, headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_number.assert_called_with('+34111111111')
        mock_slack_service.post_slack_message.assert_has_calls([call('ABCD', 'Hello, World'),
                                                                call('EFGH', 'Hello, World')])
        mock_slack_service.create_slack_channel.assert_called_with('34111111111')
        mock_slack_service.add_users_to_channel.assert_called_with(expected_users, 'EFGH')
        mock_mapping_service.update_mapping_channel_id.assert_called_with('+34111111111', 'EFGH')

    @mock.patch('twilio2slack.mapping_service')
    @mock.patch('twilio2slack.twilio_service')
    def test_slack_message_existing_mapping(self, mock_twilio_service, mock_mapping_service):
        """ Test simulating a message from Slack on an existing channel. Mapping should be retrieved from database
        and posted to Twilio"""
        mock_mapping_service.get_mapping_from_slack_channel_id.return_value = NumberToChannelMapping('+34111111111',
                                                                                                     'C2147483705')

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "message",
                "channel": "C2147483705",
                "user": "U2147483697",
                "text": "Hello, world",
                "ts": "1355517523.000005"
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_slack_channel_id.assert_called_with('C2147483705')
        mock_twilio_service.send_sms.assert_called_with('+34111111111', 'tests', "Hello, world")

    @mock.patch('twilio2slack.slack_service.get_slack_channel_info')
    @mock.patch('twilio2slack.mapping_service')
    @mock.patch('twilio2slack.twilio_service')
    def test_slack_message_not_existing_mapping(self, mock_twilio_service, mock_mapping_service, mock_slack_service):
        """ Test simulating a message from Slack on an channel (name is valid phone number) that does not have any mapping.
         Mapping should be created in database and message posted to Twilio"""
        mock_mapping_service.get_mapping_from_slack_channel_id.return_value = None
        mock_slack_service.return_value = SlackChannel('C2147483705', '34613321232')
        mock_mapping_service.add_mapping.return_value = NumberToChannelMapping('34613321232', 'C2147483705')

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "message",
                "channel": "C2147483705",
                "user": "U2147483697",
                "text": "Hello, world",
                "ts": "1355517523.000005"
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_slack_channel_id.assert_called_with('C2147483705')
        mock_slack_service.assert_called_with('C2147483705')
        mock_mapping_service.add_mapping.assert_called_with('+34613321232', 'C2147483705')
        mock_twilio_service.send_sms.assert_called_with('+34613321232', 'tests', "Hello, world")

    @mock.patch('twilio2slack.slack_service.get_slack_channel_info')
    @mock.patch('twilio2slack.mapping_service')
    @mock.patch('twilio2slack.twilio_service')
    def test_slack_message_not_existing_mapping_invalid_number(self,
                                                               mock_twilio_service,
                                                               mock_mapping_service,
                                                               mock_slack_service):
        """ Test simulating a message from Slack on an channel (name is invalid phone number) that does not have any mapping.
         No mapping should be created in database and no message posted to Twilio"""
        mock_mapping_service.get_mapping_from_slack_channel_id.return_value = None
        mock_slack_service.return_value = SlackChannel('C2147483705', 'A normal channel')

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "message",
                "channel": "C2147483705",
                "user": "U2147483697",
                "text": "Hello, world",
                "ts": "1355517523.000005"
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_slack_channel_id.assert_called_with('C2147483705')
        mock_slack_service.assert_called_with('C2147483705')
        mock_mapping_service.add_mapping.assert_not_called()
        mock_twilio_service.send_sms.assert_not_called()

    @mock.patch('twilio2slack.mapping_service')
    def test_slack_ignore_bot_message(self, mock_mapping_service):
        """ Test simulating a bot message from Slack, message should be ignored """
        mock_mapping_service.get_mapping_from_slack_channel_id.return_value = None

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "message",
                "subtype": "bot_message",
                "channel": "C2147483705",
                "user": "U2147483697",
                "text": "Hello, world",
                "ts": "1355517523.000005"
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)
        mock_mapping_service.get_mapping_from_slack_channel_id.assert_not_called()

    @mock.patch('twilio2slack.mapping_service')
    def test_slack_channel_created_invalid_phone_number(self, mock_mapping_service):
        """ Test simulating a Slack channel created with an invalid phone number"""

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "channel_created",
                "channel": {
                    "id": "C024BE91L",
                    "name": "Normal channel",
                    "created": 1360782804,
                    "creator": "U024BE7LH"
                }
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)
        mock_mapping_service.get_mapping_from_number.assert_not_called()

    @mock.patch('twilio2slack.mapping_service')
    def test_slack_channel_created_no_mapping(self, mock_mapping_service):
        """ Test simulating a Slack channel created (valid phone number name) with no pre-existing mapping in
        database """
        mock_mapping_service.get_mapping_from_number.return_value = None

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "channel_created",
                "channel": {
                    "id": "C024BE91L",
                    "name": "34613321232",
                    "created": 1360782804,
                    "creator": "U024BE7LH"
                }
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_number.assert_called_with('+34613321232')
        mock_mapping_service.add_mapping.assert_called_with('+34613321232', 'C024BE91L')

    @mock.patch('twilio2slack.mapping_service')
    def test_slack_channel_created_existing_mapping(self, mock_mapping_service):
        """ Test simulating a Slack channel created (valid phone number name) with no pre-existing mapping in
        database """
        mock_mapping_service.get_mapping_from_number.return_value = NumberToChannelMapping('+34613321232', 'CBBBBE91B')

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "channel_created",
                "channel": {
                    "id": "C024BE91L",
                    "name": "34613321232",
                    "created": 1360782804,
                    "creator": "U024BE7LH"
                }
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.get_mapping_from_number.assert_called_with('+34613321232')
        mock_mapping_service.add_mapping.assert_not_called()

    @mock.patch('twilio2slack.mapping_service')
    def test_slack_channel_deleted_existing_mapping(self, mock_mapping_service):
        """ Test simulating a Slack channel deleted with a pre-existing mapping in
        database """

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "channel_deleted",
                "channel": "C024BE91L"
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)

        mock_mapping_service.delete_mapping_by_channel_id.assert_called_with('C024BE91L')

    @mock.patch('twilio2slack.mapping_service')
    def test_slack_channel_archived_existing_mapping(self, mock_mapping_service):
        """ Test simulating a Slack channel archived (unknown event)"""

        slack_data = {
            "token": "tests",
            "type": "event_callback",
            "event": {
                "type": "channel_archive",
                "channel": "C024BE91L"
            }
        }

        response = self.app.post('/slack',
                                 data=json.dumps(slack_data),
                                 content_type='application/json',
                                 headers=headers)

        self.assertEqual(response.status_code, 200)

    @mock.patch('twilio2slack.slack_service')
    def test_add_all_users(self, mock_slack_service):
        """Test call to /addallusers: all users returned by list users should be added to all channels returned by
        list channels"""
        slack_users = [SlackUser('AAAAAA', 'jajohnso'), SlackUser('BBBBBB', 'jjohnso')]
        slack_channels = [SlackChannel('CCCCC', 'test_channel'),
                          SlackChannel('DDDDD', 'test_channel2')]

        mock_slack_service.list_users.return_value = slack_users
        mock_slack_service.list_slack_channels.return_value = slack_channels

        response = self.app.get('/addallusers', content_type='application/json', headers=headers)

        self.assertEqual(response.status_code, 200)
        mock_slack_service.add_users_to_channel.assert_has_calls([call(slack_users, 'CCCCC'),
                                                                  call(slack_users, 'DDDDD')])

    """
       Twilio service tests
    """

    @mock.patch('twilio.rest.Client.messages')
    def test_send_sms(self, mock_twilio):
        mock_logger = mock.create_autospec(Logger)

        twilio_service = TwilioService('12234', '1234', mock_logger)
        twilio_service.send_sms('666666', '777777', 'hello')
        mock_twilio.create.assert_called_with(to='666666', from_='777777', body='hello')

    @mock.patch('twilio.rest.Client.messages')
    def test_send_sms_error(self, mock_twilio):
        mock_logger = mock.create_autospec(Logger)
        mock_twilio.side_effect = TwilioRestException(400, 'anything')

        twilio_service = TwilioService('12234', '1234', mock_logger)
        twilio_service.send_sms('666666', '777777', 'hello')

    """
       Slack service tests
    """

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_create_slack_channel_not_exists(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            u'ok': True,
            u'original_team': {},
            u'channel': {
                u'topic': {},
                u'is_general': False,
                u'name_normalized': u'test',
                u'name': u'test',
                u'is_channel': True,
                u'created': 1495822888,
                u'is_member': True,
                u'is_archived': False,
                u'creator': u'U5G4WHCQ1',
                u'is_org_shared': False,
                u'unread_count': 0,
                u'previous_names': [],
                u'purpose': {},
                u'unread_count_display': 0,
                u'last_read': u'0000000000.000000',
                u'latest': None,
                u'members': [],
                u'id': u'C5J2492AU',
                u'is_shared': False
            }
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        slack_channel = slack_service.create_slack_channel('test')

        mock_api_call.assert_called_with('channels.create', name='test')

        self.assertTrue(isinstance(slack_channel, SlackChannel))
        self.assertEqual(slack_channel.channel_id, 'C5J2492AU')
        self.assertEqual(slack_channel.channel_name, 'test')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_create_slack_channel_already_exists(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            u'ok': False,
            u'error': u'name_taken'
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackChannelNameTakenException, slack_service.create_slack_channel, 'test')

        mock_api_call.assert_called_with('channels.create', name='test')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_create_slack_channel_error(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            u'ok': False,
            u'error': u'other'
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackChannelCreationException, slack_service.create_slack_channel, 'test')

        mock_api_call.assert_called_with('channels.create', name='test')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_get_slack_channel_info(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            "ok": True,
            "channel": {
                "id": "C024BE91L",
                "name": "fun",

                "created": 1360782804,
                "creator": "U024BE7LH",

                "is_archived": False,
                "is_general": False,
                "is_member": True,
                "is_starred": True,

                "members": [],

                "topic": {},
                "purpose": {},

                "last_read": "1401383885.000061",
                "latest": {},
                "unread_count": 0,
                "unread_count_display": 0
            }
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        slack_channel = slack_service.get_slack_channel_info('C024BE91L')

        mock_api_call.assert_called_with('channels.info', channel='C024BE91L')

        self.assertTrue(isinstance(slack_channel, SlackChannel))
        self.assertEqual(slack_channel.channel_id, 'C024BE91L')
        self.assertEqual(slack_channel.channel_name, 'fun')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_get_slack_channel_info_error(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            u'ok': False,
            u'error': u'other'
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackException, slack_service.get_slack_channel_info, 'C024BE91L')
        mock_api_call.assert_called_with('channels.info', channel='C024BE91L')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_list_slack_channels(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            "ok": True,
            "channels": [
                {
                    "id": "C024BE91L",
                    "name": "fun",
                    "created": 1360782804,
                    "creator": "U024BE7LH",
                    "is_archived": False,
                    "is_member": False,
                    "num_members": 6,
                    "topic": {
                        "value": "Fun times",
                        "creator": "U024BE7LV",
                        "last_set": 1369677212
                    },
                    "purpose": {
                        "value": "This channel is for fun",
                        "creator": "U024BE7LH",
                        "last_set": 1360782804
                    }
                },
                {
                    "id": "C024BE91B",
                    "name": "fun2",
                    "created": 1360782804,
                    "creator": "U024BE7LH",
                    "is_archived": False,
                    "is_member": False,
                    "num_members": 6,
                    "topic": {
                        "value": "Fun times",
                        "creator": "U024BE7LV",
                        "last_set": 1369677212
                    },
                    "purpose": {
                        "value": "This channel is for fun",
                        "creator": "U024BE7LH",
                        "last_set": 1360782804
                    }
                }
            ]
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        slack_channels = slack_service.list_slack_channels()

        mock_api_call.assert_called_with('channels.list', exclude_archived=True, exclude_members=True)

        self.assertTrue(isinstance(slack_channels, list))

        self.assertTrue(len(slack_channels) == 2)
        self.assertTrue(isinstance(slack_channels[0], SlackChannel))
        self.assertTrue(isinstance(slack_channels[1], SlackChannel))

        self.assertEqual(slack_channels[0].channel_id, 'C024BE91L')
        self.assertEqual(slack_channels[0].channel_name, 'fun')

        self.assertEqual(slack_channels[1].channel_id, 'C024BE91B')
        self.assertEqual(slack_channels[1].channel_name, 'fun2')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_list_slack_channels_error(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            u'ok': False,
            u'error': u'other'
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackException, slack_service.list_slack_channels)
        mock_api_call.assert_called_with('channels.list', exclude_archived=True, exclude_members=True)

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_post_slack_message(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            "ok": True,
            "ts": "1405895017.000506",
            "channel": "C024BE91L",
            "message": {}
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        slack_service.post_slack_message('C024BE91L', 'hello')

        mock_api_call.assert_called_with('chat.postMessage', channel='C024BE91L', text='hello')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_post_slack_message_error(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            u'ok': False,
            u'error': u'other'
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackException, slack_service.post_slack_message, 'C024BE91L', 'hello')
        mock_api_call.assert_called_with('chat.postMessage', channel='C024BE91L', text='hello')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_post_slack_message_channel_not_found(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            u'ok': False,
            u'error': u'channel_not_found'
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackChannelNotFoundException, slack_service.post_slack_message, 'C024BE91L', 'hello')
        mock_api_call.assert_called_with('chat.postMessage', channel='C024BE91L', text='hello')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_post_slack_message_channel_archived(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            u'ok': False,
            u'error': u'is_archived'
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackChannelArchivedException, slack_service.post_slack_message, 'C024BE91L', 'hello')
        mock_api_call.assert_called_with('chat.postMessage', channel='C024BE91L', text='hello')

    def test_verify_token(self):
        slack_service = SlackService('ABCD', 'EFGH', None)
        self.assertTrue(slack_service.verify_token('EFGH'))

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_list_users(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            "ok": True,
            "members": [
                {
                    "id": "AAAAAAAAA",
                    "name": "bobby1"
                },
                {
                    "id": "BBBBBBBBB",
                    "name": "bobby2"
                },
                {
                    "id": "CCCCCCCCC",
                    "name": "bobby3"
                }
            ]
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        slack_user_list = slack_service.list_users()

        mock_api_call.assert_called_with('users.list')

        self.assertTrue(isinstance(slack_user_list, list))

        self.assertTrue(len(slack_user_list) == 3)
        self.assertTrue(isinstance(slack_user_list[0], SlackUser))
        self.assertTrue(isinstance(slack_user_list[1], SlackUser))
        self.assertTrue(isinstance(slack_user_list[2], SlackUser))

        self.assertEqual(slack_user_list[0].user_id, 'AAAAAAAAA')
        self.assertEqual(slack_user_list[0].user_name, 'bobby1')

        self.assertEqual(slack_user_list[1].user_id, 'BBBBBBBBB')
        self.assertEqual(slack_user_list[1].user_name, 'bobby2')

        self.assertEqual(slack_user_list[2].user_id, 'CCCCCCCCC')
        self.assertEqual(slack_user_list[2].user_name, 'bobby3')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_list_users_exception(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            "ok": False,
            "error": "not_authed"
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackException, slack_service.list_users)

        mock_api_call.assert_called_with('users.list')

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_channels_invite(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            "ok": True
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        slack_service.invite_user_to_channel("AAAAAAA", "BBBBBB")

        mock_api_call.assert_called_with('channels.invite', channel="BBBBBB", user="AAAAAAA")

    @mock.patch.object(slackclient.SlackClient, 'api_call')
    def test_channels_invite_exception(self, mock_api_call):
        mock_logger = mock.create_autospec(Logger)
        mock_api_call.return_value = {
            "ok": False,
            "error": "channel_not_found"
        }

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)
        self.assertRaises(SlackException, slack_service.invite_user_to_channel, user_id="AAAAAAA", channel_id="BBBBBB")

    @mock.patch.object(SlackService, 'invite_user_to_channel')
    def test_add_users_to_channel(self, mock_invite_user_to_channel):
        mock_logger = mock.create_autospec(Logger)

        slack_service = SlackService('ABCD', 'EFGH', mock_logger)

        user_list = [SlackUser('AAAAAAA', 'jajohnso'), SlackUser('BBBBBB', 'test'),
                     SlackUser('CCCCCC', 'freddie')]

        slack_service.add_users_to_channel(user_list, 'DDDDDDD')

        mock_invite_user_to_channel.assert_has_calls([call('AAAAAAA', 'DDDDDDD'),
                                                      call('BBBBBB', 'DDDDDDD'),
                                                      call('CCCCCC', 'DDDDDDD')])

if __name__ == '__main__':
    unittest.main()
