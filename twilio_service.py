from twilio.base.exceptions import TwilioRestException
from twilio.rest import Client


class TwilioService:
    def __init__(self, twilio_account_sid, twilio_auth_token, logger):
        self.twilio_client = Client(twilio_account_sid, twilio_auth_token)
        self.logger = logger

    def send_sms(self, to_number, from_number, body):
        try:
            self.twilio_client.messages.create(to=to_number, from_=from_number, body=body)
            self.logger.debug("Successfully sent request to Twilio (to=%s, from=%s, message=%s)",
                              to_number, from_number, body)

        except TwilioRestException as e:
            self.logger.error("Failure sending request to Twilio (to=%s, from=%s, message=%s): %s",
                              to_number, from_number, body, e)
