from slackclient import SlackClient


class SlackService:
    def __init__(self, slack_api_token, slack_verification_token, logger):
        self.slack_client = SlackClient(slack_api_token)
        self.slack_verification_token = slack_verification_token
        self.logger = logger

    def create_slack_channel(self, channel_name):
        self.logger.debug("Calling channels.create with name=%s", channel_name)
        slack_response = self.slack_client.api_call('channels.create', name=channel_name)
        self.logger.debug("Slack response: %s", slack_response)

        if not self.slack_response_ok(slack_response):
            msg = "Error creating Slack channel(name=%s), error: %s" % \
                  (channel_name, self.get_slack_error(slack_response))

            self.logger.debug(msg)

            if slack_response['error'] == 'name_taken':
                raise SlackChannelNameTakenException(msg)
            else:
                raise SlackChannelCreationException(msg)

        return SlackChannel(slack_response['channel']['id'], slack_response['channel']['name'])

    def get_slack_channel_info(self, channel_id):
        self.logger.debug("Calling channels.info with channel=%s", channel_id)
        slack_response = self.slack_client.api_call('channels.info', channel=channel_id)
        self.logger.debug("Slack response: %s", slack_response)

        if not self.slack_response_ok(slack_response):
            msg = "Error getting Slack channel info (channel=%s), error: %s" % (channel_id,
                                                                                self.get_slack_error(slack_response))
            self.logger.debug(msg)
            raise SlackException(msg)

        return SlackChannel(slack_response['channel']['id'], slack_response['channel']['name'])

    def list_slack_channels(self, exclude_archived=True, exclude_members=True):
        self.logger.debug("Calling channels.list with exclude_archived=%s and exclude_members=%s",
                          exclude_archived,
                          exclude_members)

        slack_response = self.slack_client.api_call('channels.list',
                                                    exclude_archived=exclude_archived,
                                                    exclude_members=exclude_members)

        self.logger.debug("Slack response: %s", slack_response)

        if not self.slack_response_ok(slack_response):
            msg = "Error getting Slack channel list, error: %s" % self.get_slack_error(slack_response)
            self.logger.debug(msg)
            raise SlackException(msg)

        return [SlackChannel(channel['id'], channel['name']) for channel in slack_response['channels']]

    def post_slack_message(self, channel_id, message):
        self.logger.debug("Calling chat.postMessage with channel=%s, text=%s", channel_id, message)
        slack_response = self.slack_client.api_call(
            "chat.postMessage",
            channel="%s" % channel_id,
            text=message
        )
        self.logger.debug("Slack response: %s", slack_response)

        if not self.slack_response_ok(slack_response):
            msg = "Error posting message to Slack (channel=%s, message=%s), error: %s" \
                  % (channel_id, message, self.get_slack_error(slack_response))
            self.logger.debug(msg)

            if slack_response['error'] == 'channel_not_found':
                raise SlackChannelNotFoundException(msg)
            elif slack_response['error'] == 'is_archived':
                raise SlackChannelArchivedException(msg)
            else:
                raise SlackPostMessageException(msg)

    def invite_user_to_channel(self, user_id, channel_id):
        self.logger.debug("Calling channels.invite with user_id=%s, channel_id=%s", user_id, channel_id)
        slack_response = self.slack_client.api_call("channels.invite", channel=channel_id, user=user_id)
        self.logger.debug("Slack response: %s", slack_response)

        if not self.slack_response_ok(slack_response):
            msg = "Error inviting user %s to channel %s (message=%s)" \
                  % (user_id, channel_id, self.get_slack_error(slack_response))
            self.logger.debug(msg)

            if slack_response['error'] != 'cant_invite_self' and slack_response['error'] != 'already_in_channel' \
                    and slack_response['error'] != 'cant_invite':
                raise SlackException(msg)

    def list_users(self):
        self.logger.debug("Calling users.list")
        slack_response = self.slack_client.api_call("users.list")
        self.logger.debug("Slack response: %s", slack_response)

        if not self.slack_response_ok(slack_response):
            msg = "Error getting list of Slack users (message=%s)" % self.get_slack_error(slack_response)
            self.logger.debug(msg)
            raise SlackException(msg)

        return [SlackUser(user['id'], user['name']) for user in slack_response['members']]

    def add_users_to_channel(self, user_list, channel_id):
        for user in user_list:
            self.logger.info("Inviting %s (id=%s) to channel %s", user.user_name, user.user_id, channel_id)
            try:
                self.invite_user_to_channel(user.user_id, channel_id)
            except SlackException as e:
                self.logger.warn("Error inviting user %s to Slack channel %s", user.user_id, channel_id)

    def verify_token(self, slack_verification_token):
        return self.slack_verification_token == slack_verification_token

    @staticmethod
    def slack_response_ok(slack_response):
        return slack_response['ok'] is True

    @staticmethod
    def get_slack_error(slack_response):
        return slack_response['error']


class SlackChannel:
    def __init__(self, channel_id, channel_name):
        self.channel_id = channel_id
        self.channel_name = channel_name


class SlackUser:
    def __init__(self, user_id, user_name):
        self.user_id = user_id
        self.user_name = user_name


class SlackException(Exception):
    pass


class SlackChannelCreationException(SlackException):
    pass


class SlackChannelNameTakenException(SlackChannelCreationException):
    pass


class SlackPostMessageException(SlackException):
    pass


class SlackChannelNotFoundException(SlackPostMessageException):
    pass


class SlackChannelArchivedException(SlackPostMessageException):
    pass
