#!/usr/bin/env python
import os
from wsgiref.handlers import CGIHandler

os.environ['TWILIO_ACCOUNT_SID'] = ''
os.environ['TWILIO_AUTH_TOKEN'] = ''
os.environ['TWILIO_PHONE_NUMBER'] = ''
os.environ['SLACK_API_TOKEN'] = ''
os.environ['SLACK_VERIFICATION_TOKEN'] = ''

from twilio2slack import app

CGIHandler().run(app)