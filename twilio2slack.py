#!/usr/bin/env python

import logging
import os
from logging.handlers import RotatingFileHandler

import phonenumbers
from flask import Flask, request
from phonenumbers import NumberParseException
from flask_httpauth import HTTPBasicAuth

from database import db_session
from database import init_db
from mapping_service import MappingService
from slack_service import SlackService, SlackChannelNameTakenException, SlackException, \
    SlackChannelCreationException, SlackPostMessageException, SlackChannelNotFoundException
from twilio_service import TwilioService

TWILIO_ACCOUNT_SID_KEY = 'TWILIO_ACCOUNT_SID'
TWILIO_AUTH_TOKEN_KEY = 'TWILIO_AUTH_TOKEN'
TWILIO_PHONE_NUMBER_KEY = 'TWILIO_PHONE_NUMBER'
SLACK_API_TOKEN_KEY = 'SLACK_API_TOKEN'
SLACK_VERIFICATION_TOKEN_KEY = 'SLACK_VERIFICATION_TOKEN'

app = Flask(__name__)
auth = HTTPBasicAuth()

handler = RotatingFileHandler('application.log', maxBytes=100000, backupCount=10)
handler.setLevel(logging.DEBUG)
app.logger.addHandler(handler)

if TWILIO_ACCOUNT_SID_KEY not in os.environ or TWILIO_AUTH_TOKEN_KEY not in os.environ or TWILIO_PHONE_NUMBER_KEY not in os.environ:
    app.logger.error("%s, %s and %s must be set as environment variables",
                     TWILIO_ACCOUNT_SID_KEY, TWILIO_AUTH_TOKEN_KEY, TWILIO_PHONE_NUMBER_KEY)
    exit(1)

if SLACK_API_TOKEN_KEY not in os.environ or SLACK_VERIFICATION_TOKEN_KEY not in os.environ:
    app.logger.error("%s and %s must be set as environment variables",
                     SLACK_API_TOKEN_KEY, SLACK_VERIFICATION_TOKEN_KEY)
    exit(1)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

init_db()

twilio_account_sid = os.environ[TWILIO_ACCOUNT_SID_KEY].strip()
twilio_auth_token = os.environ[TWILIO_AUTH_TOKEN_KEY].strip()
twilio_phone_number = os.environ[TWILIO_PHONE_NUMBER_KEY].strip()

slack_api_token = os.environ[SLACK_API_TOKEN_KEY].strip()
slack_verification_token = os.environ[SLACK_VERIFICATION_TOKEN_KEY].strip()

slack_service = SlackService(slack_api_token, slack_verification_token, app.logger)
mapping_service = MappingService(app.logger)
twilio_service = TwilioService(twilio_account_sid, twilio_auth_token, app.logger)

users = {
    "twilio2slack": "updateme"
}


@auth.get_password
def get_pw(username):
    if username in users:
        return users.get(username)
    return None


@app.route('/sms', methods=['GET', 'POST'])
def twilio_sms():
    app.logger.debug("Received SMS from Twilio %s", request.form)

    sms_from = request.form['From']
    sms_body = request.form['Body']

    number_to_channel_mapping = mapping_service.get_mapping_from_number(sms_from)

    slack_channel_name = sms_from.replace('+', '')

    if not number_to_channel_mapping:
        app.logger.info("No existing Slack channel for %s, creating...", sms_from)

        try:
            slack_channel = slack_service.create_slack_channel(slack_channel_name)
            app.logger.info("Created Slack channel %s with id %s", slack_channel.channel_name, slack_channel.channel_id)

            try:
                user_list = slack_service.list_users()
                app.logger.info("Adding all team users to channel %s", slack_channel.channel_name)
                slack_service.add_users_to_channel(user_list, slack_channel.channel_id)
            except SlackException as e:
                app.logger.warn("Error while adding all team members to new Slack channel %s",
                                slack_channel.channel_name)

        except SlackChannelNameTakenException:
            # DB is out of sync, try to recover
            app.logger.warn("Tried to create Slack channel %s but name was taken, recovering ID of existing channel",
                            slack_channel_name)
            try:
                slack_channel_list = slack_service.list_slack_channels()
                slack_channel = next((channel for channel in slack_channel_list
                                      if channel.channel_name == slack_channel_name),
                                     None)
            except SlackException as e:
                app.logger.error("Could not recover ID of existing Slack channel with name %s." +
                                 " Message from %s not sent to Slack. Error msg: %s",
                                 slack_channel_name, sms_from, e.message)

                return e.message, 500

        except SlackChannelCreationException as e:
            app.logger.error("Could not create Slack channel with name %s for %s. Error msg: %s",
                             slack_channel_name, sms_from, e.message)
            return e.message, 500

        app.logger.info("Associating number %s to Slack channel_id %s", sms_from, slack_channel.channel_id)
        number_to_channel_mapping = mapping_service.add_mapping(sms_from, slack_channel.channel_id)
    else:
        app.logger.debug("Found existing mapping for %s -> channel_id=%s",
                         sms_from, number_to_channel_mapping.channel_id)

    try:
        slack_service.post_slack_message(number_to_channel_mapping.channel_id, sms_body)
        app.logger.debug("Successfully posted message from %s to Slack channel_id %s",
                         number_to_channel_mapping.number,
                         number_to_channel_mapping.channel_id)
    except SlackChannelNotFoundException:
        app.logger.warn("Mapping from %s to Slack channel_id %s is wrong, channel does not exists. Creating new channel"
                        , sms_from, number_to_channel_mapping.channel_id)
        try:
            new_slack_channel = slack_service.create_slack_channel(slack_channel_name)

            try:
                user_list = slack_service.list_users()
                app.logger.info("Adding all team users to channel %s", new_slack_channel.channel_name)
                slack_service.add_users_to_channel(user_list, new_slack_channel.channel_id)
            except SlackException as e:
                app.logger.warn("Error while adding all team members to new Slack channel %s",
                                new_slack_channel.channel_name)

        except SlackChannelCreationException as e:
            app.logger.error("Could not create Slack channel with name %s for %s. Error msg: %s",
                             slack_channel_name, sms_from, e.message)
            return e.message, 500

        app.logger.info("Associating number %s to Slack channel_id %s", sms_from, new_slack_channel.channel_id)
        # update mapping in local DB
        new_number_to_channel_mapping = mapping_service.update_mapping_channel_id(sms_from,
                                                                                  new_slack_channel.channel_id)

        try:
            slack_service.post_slack_message(new_number_to_channel_mapping.channel_id, sms_body)
        except SlackPostMessageException as e:
            app.logger.error("Error posting message from %s to Slack channel_id %s. Error msg: %s",
                             sms_from, new_number_to_channel_mapping.channel_id, e.message)
            return e.message, 500
    except SlackPostMessageException as e:
        app.logger.error("Error posting message from %s to Slack channel_id %s. Error msg: %s",
                         sms_from, number_to_channel_mapping, e.message)
        return e.message, 500

    return '', 200


@app.route('/slack', methods=['POST'])
def slack_event():
    app.logger.debug("Received Slack event %s", request.json)

    # verify token
    if 'token' not in request.json or not slack_service.verify_token(request.json['token']):
        app.logger.warn('Verification token received from Slack ' +
                        'did not match configured verification token (SLACK_VERIFICATION_TOKEN)')
        return '', 200

    event_type = request.json['type']

    if event_type == 'url_verification':
        return request.json['challenge']
    elif event_type == 'event_callback':
        event = request.json['event']

        if event['type'] == 'message':
            app.logger.debug('Event type is message')
            process_slack_message_event(event)

        elif event['type'] == 'channel_created':
            app.logger.debug('Event type is channel created')
            process_slack_channel_created_event(event)

        elif event['type'] == 'channel_deleted':
            app.logger.debug('Event type is channel deleted')
            process_slack_channel_deleted_event(event)

        else:
            app.logger.warn("No action was defined for received Slack event type %s, no actions taken", event['type'])
    else:
        app.logger.warn("No action was defined for received Slack event type %s, no actions taken", event_type)

    return '', 200


@app.route('/addallusers', methods=['GET'])
def add_all_users():
    app.logger.debug("Received call to add all users")

    try:
        slack_channels = slack_service.list_slack_channels()
        slack_users = slack_service.list_users()
    except SlackException as e:
        app.logger.error("Error adding all users to all channels, msg=%1", e.message)
        return 'Something went wrong, message=%s' % e.message, 500

    for channel in slack_channels:
        slack_service.add_users_to_channel(slack_users, channel.channel_id)

    return 'Successfully added all team to all channels', 200


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


def process_slack_message_event(event):
    if 'subtype' in event:
        app.logger.debug('Received message was not chat message (bot or channel joined), ignoring...')
    else:
        message = event['text']

        channel_id = event['channel']

        app.logger.debug('Processing message from Slack channel_id %s', channel_id)

        number_to_channel_mapping = mapping_service.get_mapping_from_slack_channel_id(channel_id)

        if number_to_channel_mapping:
            app.logger.debug("Slack channel id %s is associated to number %s. Sending Twilio message...",
                             channel_id, number_to_channel_mapping.number)
            twilio_service.send_sms(number_to_channel_mapping.number, twilio_phone_number, message)
        else:
            app.logger.info("Slack channel id %s is not associated to any number. Either DB is out of sync, or " +
                            " channel was created manually in Slack interface", channel_id)
            app.logger.info("If Slack channel with ID %s has a valid phone number name it will be mapped to this " +
                            " phone number", channel_id)
            # mapping does not exist but we can create it if the channel name is a valid international phone number
            slack_channel = slack_service.get_slack_channel_info(channel_id)

            phone_number = '+' + slack_channel.channel_name
            try:
                number = phonenumbers.parse(phone_number)
            except NumberParseException:
                app.logger.debug('Channel name (%s) is not a valid international phone number (%s). No mapping created',
                                 slack_channel.channel_name, phone_number)
                return

            if not phonenumbers.is_valid_number(number):
                app.logger.warn('Channel name (%s) is not a valid phone number (%s). No mapping created',
                                slack_channel.channel_name, phone_number)
            else:
                app.logger.info("Slack channel id %s is has a valid phone number name. Creating mapping from " +
                                " Slack channel %s to number %s", channel_id, slack_channel.channel_name, number)
                # we can create the mapping and send the message
                mapping_service.add_mapping(phone_number, slack_channel.channel_id)
                twilio_service.send_sms(phone_number, twilio_phone_number, message)


def process_slack_channel_created_event(event):
    channel = event['channel']
    channel_id = channel['id']
    channel_name = channel['name']

    app.logger.info("Processing Slack channel created event (channel_id=%s, name=%s), if channel name is valid phone " +
                    "number, mapping will be created", channel_id, channel_name)

    phone_number = '+' + channel_name
    try:
        number = phonenumbers.parse(phone_number)
    except NumberParseException:
        app.logger.debug('Channel name (%s) is not a valid international phone number (%s). No mapping created',
                         channel_name, phone_number)
        return

    if not phonenumbers.is_valid_number(number):
        app.logger.debug('Channel name (%s) is not a valid international phone number (%s). No mapping created',
                         channel_name, phone_number)
    else:
        app.logger.info("Slack channel id %s is has a valid phone number name. Creating mapping from " +
                        " Slack channel %s to number %s", channel_id, channel_name, phone_number)

        number_to_channel_mapping = mapping_service.get_mapping_from_number(phone_number)

        if number_to_channel_mapping:
            # mapping already exists
            app.logger.warn("Number %s is already mapped to channel_id %s", phone_number, channel_id)
        else:
            # store mapping in local DB
            mapping_service.add_mapping(phone_number, channel_id)


def process_slack_channel_deleted_event(event):
    app.logger.info("Processing Slack channel deleted event, deleting any mapping for channel_id %s", event['channel'])
    mapping_service.delete_mapping_by_channel_id(event['channel'])


if __name__ == "__main__":
    app.run()
