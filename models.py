from sqlalchemy import Column, String

from database import Base


class NumberToChannelMapping(Base):
    __tablename__ = 'number_to_channel_mapping'

    number = Column(String(20), primary_key=True)
    channel_id = Column(String(20), unique=True)

    def __init__(self, number, channel_id):
        self.number = number
        self.channel_id = channel_id

    def __str__(self):
        return "NumberToChannelMapping(number=%s, channel_id=%s)" % \
               (self.number, self.channel_id)
